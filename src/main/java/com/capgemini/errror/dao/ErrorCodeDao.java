package com.capgemini.errror.dao;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.capgemini.error.model.ErrorCode;

public interface ErrorCodeDao {

	public ErrorCode findByExceptionConfigID(int exceptionConfigID);
	
	public ArrayList<ErrorCode> getAllServices();
	
	public ErrorCode getService();
}
