package com.capgemini.error.daoImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.error.model.ErrorCode;
import com.capgemini.errror.dao.ErrorCodeDao;

@Component
public class ErrorCodeDaoImpl implements ErrorCodeDao {
	@Autowired
	private DataSource dataSource;

	@Autowired
	private Properties queryProps;

	public ErrorCode findByExceptionConfigID(int exceptionConfigID) {
		Connection conn = null;

		try {

			conn = dataSource.getConnection();
			// System.out.println(dataSource);
			Statement stmt = conn.createStatement();
			ErrorCode errorCode = null;

			// Executing the statement
			ResultSet rs = stmt.executeQuery("SELECT * FROM ERRORCODE_CROSS_REF WHERE EXCEPTION_CONFIG_ID = 5");
			if (rs.next()) {
				errorCode = new ErrorCode(rs.getInt("EXCEPTION_CONFIG_ID"), rs.getString("CHANNEL_STATUS_CODE"),
						rs.getInt("HTTP_STATUS_CODE"), rs.getString("VALIDATION_TYPE"), rs.getString("DESCRIPTION"));
			}
			rs.close();
			stmt.close();
			return errorCode;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}

		}

	}

	@Override
	public ArrayList<ErrorCode> getAllServices() {

	List<ErrorCode> errorCodeList = new ArrayList<ErrorCode>();
			
			String queryStmt = queryProps.getProperty("getAllServices");
			
			System.out.println(queryStmt);
			Connection conn = null;
			try{
				conn= dataSource.getConnection();
				PreparedStatement pstmt= conn.prepareStatement(queryStmt);
				ErrorCode errorCode = new ErrorCode();
				ResultSet rs = pstmt.executeQuery();
				
				if(rs.next()){
					errorCode.setExceptionConfigId(rs.getInt("EXCEPTION_CONFIG_ID"));
					errorCode.setChannelStatusCode(rs.getString("CHANNEL_STATUS_CODE"));
					errorCode.setHttpStatusCode(rs.getInt("HTTP_STATUS_CODE"));
					errorCode.setValidationType(rs.getString("VALIDATION_TYPE"));
					errorCode.setDescription(rs.getString("DESCRIPTION"));
					
					errorCodeList.add(errorCode);
				}
				rs.close();
				pstmt.close();
			}
			catch (SQLException e) {
				throw new RuntimeException(e);
			}
			finally{
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
					}
				}	
		}
			
			return (ArrayList<ErrorCode>) errorCodeList;	
	}
	
	


	@Override
	public ErrorCode getService() {
		// TODO Auto-generated method stub
		return null;
	}

}
