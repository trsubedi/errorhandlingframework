package com.capgemini.error.model;

public class ErrorCode {
	int exceptionConfigId;
	String channelStatusCode;
	int httpStatusCode;
	String validationType;
	String Description;
	
	public ErrorCode(){
		
	}
	
	public String toString(){
		
		String returnValue = "channelStatusCode=" + channelStatusCode + "|validationType=" + validationType;
		return returnValue;
		
	}
	
	public ErrorCode(int exceptionConfigID, String channelStatusCode, int httpStatusCode, String validationType, String Description) {
			this.exceptionConfigId = exceptionConfigID;
			this.channelStatusCode = channelStatusCode;
			this.httpStatusCode = httpStatusCode;
			this.validationType = validationType;
			this.Description = Description;	
	}
	
	public int getExceptionConfigId() {
		return exceptionConfigId;
	}
	public void setExceptionConfigId(int exceptionConfigId) {
		this.exceptionConfigId = exceptionConfigId;
	}
	public String getChannelStatusCode() {
		return channelStatusCode;
	}
	public void setChannelStatusCode(String channelStatusCode) {
		this.channelStatusCode = channelStatusCode;
	}
	public int getHttpStatusCode() {
		return httpStatusCode;
	}
	public void setHttpStatusCode(int httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}
	public String getValidationType() {
		return validationType;
	}
	public void setValidationType(String validationType) {
		this.validationType = validationType;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	
	
	
	

}
