package com.capgemini.test;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.capgemini.error.daoImpl.ErrorCodeDaoImpl;
import com.capgemini.error.model.ErrorCode;
import com.capgemini.errror.dao.ErrorCodeDao;

public class TestApplication {
	
	public static void main(String[] args){
		ApplicationContext context =
	    		new ClassPathXmlApplicationContext("Spring.xml");			
		
		ErrorCodeDao errorCodeDaoImpl= (ErrorCodeDao)context.getBean("errorCodeDaoImpl");
		
//		ErrorCode ec = errorCodeDaoImpl.findByExceptionConfigID(1);
//		System.out.println(ec.getValidationType());
		
		List<ErrorCode> ec = errorCodeDaoImpl.getAllServices();
		
//		for(ErrorCode obj:ec){
//			System.out.println(obj);
		
		System.out.println(ec.toString());
		}
	}
